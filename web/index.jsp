<%--
  Created by IntelliJ IDEA.
  User: oscar
  Date: 2019-05-25
  Time: 17:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Mastermind API</title>
  </head>
  <body>
  <h1>Mastermind is a code-breaking game for two players.</h1>
  <br/>
  One player becomes the codemaker, the other the codebreaker. The codemaker chooses a pattern of four color code pegs (duplicates allowed) and the codebreaker tries to guess it, in both order and color.
  Each guess is made by placing a row of color code pegs on the decoding board. Once placed, the codemaker provides feedback by placing from zero to four key pegs in the small holes of the row with the guess. A black key peg (small red in the image) is placed for each code peg from the guess which is correct in both color and position. A white key peg indicates the existence of a correct color code peg placed in the wrong position.
  <br/>Example: Given a code [RED, BLUE, GREEN, RED] when the codebreaker gives a code with [RED, GREEN, RED, YELLOW] the feedback will be: 1 black, 2 whites.
  <br/>For more information about the game: https://en.wikipedia.org/wiki/Mastermind_(board_game)<hr/>

  <h2>Create a game </h2>
  Use POST /new-game to create the game
  Returns gameid, which will be used when requesting a guess evaluation

  <h2> Make a guess </h2>
  <p>Make a guess with POST /guess</p>
  <p><b>BODY:</b> {"gameid":"gameid_provide_on_create","code": [color1, color2, color3, color4]}  <br/>
  Example: {"gameid":"0","code": ["RED", "GREEN", "RED", "YELLOW"]}  <br/></p>

  <p><b>RESPONSE:</b> {"result": "<X> black, <Y> whites"}  <br/>
    Example: {"result": "1 black, 2 whites"}  <br/></p>

  <h2>Get the history log of the game </h2>
  Use GET /history to get the list of plays

  </body>
</html>
