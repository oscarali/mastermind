package com.oc.mastermind.core;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class SimpleMastermindCodeMakerTest {

    private SimpleMastermindCodeMaker mastermindCodeMaker = new SimpleMastermindCodeMaker();
    String [] arrayCode = {"RED", "BLUE", "GREEN", "RED"};
    private String gameId = mastermindCodeMaker.createGame(arrayCode);

    @Test
    void evaluateGuess2b0w() throws GuessException {
        String [] guess = {"RED", "BLUE", "YELLOW", "ORANGE"};
        GuessResult result = this.mastermindCodeMaker.evaluateGuess(gameId,guess);
        assertEquals("2 blacks, 0 whites",result.toString());
    }

    @Test
    void evaluateGuess0b2w() throws GuessException {
        String [] guess2 = {"BLUE", "YELLOW", "RED", "ORANGE"};
        GuessResult result = this.mastermindCodeMaker.evaluateGuess(gameId,guess2);
        assertEquals("0 blacks, 2 whites",result.toString());
    }

    @Test
    void evaluateGuess1b1w() throws GuessException {
        String [] guess = {"RED", "YELLOW", "RED", "ORANGE"};
        GuessResult result = this.mastermindCodeMaker.evaluateGuess(gameId,guess);
        assertEquals("1 blacks, 1 whites",result.toString());
    }

    @Test
    void evaluateGuess2b2w() throws GuessException {
        String [] guess2 = {"BLUE", "RED", "GREEN", "RED"   };
        GuessResult result = this.mastermindCodeMaker.evaluateGuess(gameId,guess2);
        assertEquals("2 blacks, 2 whites",result.toString());
    }

    @Test
    void evaluateGuess4b0w() throws GuessException {
        String [] guess2 = {"RED", "BLUE", "GREEN", "RED"};
        GuessResult result = this.mastermindCodeMaker.evaluateGuess(gameId,guess2);
        assertEquals("4 blacks, 0 whites",result.toString());
    }

    @Test
    void evaluateGuess0b4w() throws GuessException {
        String [] guess2 = {"BLUE", "RED", "RED", "GREEN"};
        GuessResult result = this.mastermindCodeMaker.evaluateGuess(gameId,guess2);
        assertEquals("0 blacks, 4 whites",result.toString());
    }
}