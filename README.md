# README #

Rest API that simulates the role of the Masterminds codemaker.

### A simple implementation of the game Mastermind with a Restful API to interact with  ###

Mastermind is a code-breaking game for two players. One player becomes the codemaker, the other the codebreaker. The codemaker chooses a pattern of four color code pegs (duplicates allowed) and the codebreaker tries to guess it, in both order and color.
Each guess is made by placing a row of color code pegs on the decoding board. Once placed, the codemaker provides feedback by placing from zero to four key pegs in the small holes of the row with the guess. A black key peg (small red in the image) is placed for each code peg from the guess which is correct in both color and position. A white key peg indicates the existence of a correct color code peg placed in the wrong position.
Example: Given a code [RED, BLUE, GREEN, RED] when the codebreaker gives a code with [RED, GREEN, RED, YELLOW] the feedback will be: 1 black, 2 whites.

6 colors are allowed: RED, BLUE, YELLOW, ORANGE, GREEN, PURPLE

### Create a game ###
Use POST /new-game to create the game
Returns gameid, which will be used when requesting a guess evaluation

### Make a guess ###
Make a guess with POST /guess

with BODY {"gameid":"gameid_provide_on_create","code": [color1, color2, color3, color4]} 
Example: {"gameid":"0","code": ["RED", "GREEN", "RED", "YELLOW"]} 

RESPONSE: {"result": "<X> black, <Y> whites"} 
Example: {"result": "1 black, 2 whites"} 


### Get history ###

Use GET /history/id to get the list of the game of th given id
