package com.oc.mastermind.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class MastermindGame {
    private String gameId;
    private HashMap<String, HashSet<Integer>> code;
    private ArrayList<GameHistoryItem> historyLog;

    public MastermindGame(String gameId, HashMap<String, HashSet<Integer>> code) {
        this.gameId = gameId;
        this.code = code;
        this.historyLog = new ArrayList<>();
    }

    public HashMap<String, HashSet<Integer>> getCode() {
        return code;
    }

    public void saveGuessToHistoryLog(GameHistoryItem item){
        historyLog.add(item);
    }

    public String getHistoryLogString(){
        StringBuilder historyLogBuf = new StringBuilder();

        if(historyLog.size()==0)
            return "Log is empty, no guessings registered yet.";

        historyLog.forEach((item) -> historyLogBuf.append(item).append("\n"));

        return historyLogBuf.toString();
    }

    public ArrayList<GameHistoryItem> getHistoryLog(){
        return historyLog;
    }
}
