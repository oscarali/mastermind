package com.oc.mastermind.core;

import java.util.Arrays;

public class GameHistoryItem {
    private String guess;
    private String result;

    public GameHistoryItem(String [] code, GuessResult result){
        this.guess = Arrays.toString(code);
        this.result = result.toString();
    }

    @Override
    public String toString(){
        return this.guess + " -> " + this.result;
    }
}
