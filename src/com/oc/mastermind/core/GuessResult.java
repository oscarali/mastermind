package com.oc.mastermind.core;

public class GuessResult {
    private int blackKeyPegs;
    private int whiteKeyPegs;

    public GuessResult(int blackKeyPegs, int whiteKeyPegs) {
        this.blackKeyPegs = blackKeyPegs;
        this.whiteKeyPegs = whiteKeyPegs;
    }

    @Override
    public String toString() {
        return blackKeyPegs + " blacks, " + whiteKeyPegs + " whites";
    }

    public int getBlackKeyPegs() {
        return blackKeyPegs;
    }

    public int getWhiteKeyPegs() {
        return whiteKeyPegs;
    }
}
