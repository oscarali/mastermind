package com.oc.mastermind.core;

public class GuessException extends Exception {
    public GuessException() {
        super();
    }

    public GuessException(Throwable cause) {
        super(cause);
    }

    public GuessException(String message) {
        super(message);
    }

    public GuessException(String message, Throwable cause) {
        super(message, cause);
    }

    protected GuessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
