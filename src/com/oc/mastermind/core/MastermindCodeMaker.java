package com.oc.mastermind.core;

import java.util.ArrayList;

public interface MastermindCodeMaker {
    /**
     * Creates a new game and returns the ID of the game
     *
     * @return String
     */
    String createGame();

    /**
     * (For testing purposes)
     * Creates a new game with a preset code and returns the ID of the game
     *
     * @return String
     */
    String createGame(String[] arrayCode);

    /**
     * Evaluates a guess for a given game (identified with gameId) and returns feedback to the codebreaker
     *
     * @param gameId
     * @param guess
     * @return GuessResult
     */
    GuessResult evaluateGuess(String gameId, String[] guess) throws GuessException;

    /**
     * For given gameId, returns string representation of the list of guessings and results
     *
     * @param gameId
     * @return String
     */
    String getHistoryLogString(String gameId) throws GuessException;
    /**
     * For given gameId, returns string representation of the list of guessings and results
     *
     * @param gameId
     * @return String
     */
    ArrayList getHistoryLog(String gameId) throws GuessException;

}
