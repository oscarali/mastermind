package com.oc.mastermind.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicLong;

public class SimpleMastermindCodeMaker implements MastermindCodeMaker {

    private static final String[] colors = {"RED", "BLUE", "YELLOW", "ORANGE", "GREEN", "PURPLE"};
    private static int numberOfPegs = 4; // this could be a parameter of the game
    private static AtomicLong idCounter = new AtomicLong();
    private static HashMap<String, MastermindGame> games = new HashMap();

    @Override
    public String createGame() {
        String newGameId = getNewGameId();
        HashMap<String, HashSet<Integer>> code = generateCode();
        MastermindGame game = new MastermindGame(newGameId,code);
        games.put(newGameId,game);

        return newGameId;
    }

    @Override
    public String createGame(String[] arrayCode) {
        String newGameId = getNewGameId();
        HashMap<String, HashSet<Integer>> code = getCodeFromArray(arrayCode);
        MastermindGame game = new MastermindGame(newGameId,code);
        games.put(newGameId,game);

        return newGameId;
    }

    @Override
    public GuessResult evaluateGuess(String gameId, String[] guess) throws GuessException {
        if (guess.length != numberOfPegs) {
            throw new GuessException(String.format("Guess must be formed by %d colors", numberOfPegs));
        }

        try {
            MastermindGame game = getGame(gameId);
            GuessResult result = getGuessScore(game,guess);
            game.saveGuessToHistoryLog(new GameHistoryItem(guess,result));

            return result;
        } catch (Exception e) {
            throw new GuessException(String.format("Invalid Guess: %s", e.getMessage()));
        }
    }

    @Override
    public String getHistoryLogString(String gameId) throws GuessException {
        return getGame(gameId).getHistoryLogString();
    }

    @Override
    public ArrayList getHistoryLog(String gameId) throws GuessException {
        return getGame(gameId).getHistoryLog();
    }

    /**
     * Returns the Mastermind game object for a given gameid
     * @return the secret code
     */
    private MastermindGame getGame(String gameId) throws GuessException {
        MastermindGame game = games.get(gameId);
        if(game==null){
            throw new GuessException(String.format("Invalid Game Id: %s", gameId));
        }
        return game;
    }

    /**
     * Returns the Mastermind game object for a given gameid
     * @return the secret code
     */
    private String getNewGameId() {
        // The game id can be an autonumeric id, there is no need for global uniqueness, we don't need a UUID
        // The use of AtomicLong relies on under-the-hood "magic" for thread safety, but is scalable and safe
        return String.valueOf(idCounter.getAndIncrement());
    }

    /**
     * Randomly generates a code
     * @return the secret code
     */
    private HashMap<String, HashSet<Integer>> generateCode() {
        HashMap<String, HashSet<Integer>> code = new HashMap<>();
        String colorsStr = "New code: ";
        for (int i = 0; i < numberOfPegs; i++) {
            int randomIndex = ThreadLocalRandom.current().nextInt(0, colors.length);
            String color = colors[randomIndex];
            colorsStr += color+".";
            HashSet<Integer> indexes = code.getOrDefault(color, new HashSet<>());
            indexes.add(i);
            code.put(color, indexes);
        }
        // just to see it on the server's log and be able to test the successful guess
        System.err.println(colorsStr);
        return code;
    }

    /**
     * Get a hashmap representation of a string array code
     * @return the code as hashmap
     */
    private HashMap<String, HashSet<Integer>> getCodeFromArray(String[] arrayCode) {
        HashMap<String, HashSet<Integer>> code = new HashMap<>();
        for (int i = 0; i < arrayCode.length; i++) {
            String color = arrayCode[i];
            HashSet<Integer> indexes = code.getOrDefault(color, new HashSet<>());
            indexes.add(i);
            code.put(color, indexes);
        }
        return code;
    }

    /**
     * Scores the given guess
     * @param guess Guess string provided by the codebreaker
     * @return Result of the guess scoring
     */
    private GuessResult getGuessScore(MastermindGame game, String[] guess) throws GuessException {
        int blackKeyPegs = 0;
        int whiteKeyPegs = 0;
        HashMap<String, HashSet<Integer>> code = game.getCode();

        for (int i = 0; i < guess.length; i++) {
            String guessColor = guess[i].toUpperCase();
            if (isValidGuessColor(guessColor)) {
                if (code.containsKey(guessColor)) {
                    HashSet<Integer> indexes = code.get(guessColor);
                    if (indexes.contains(i)) {
                        blackKeyPegs++;
                    } else {
                        whiteKeyPegs++;
                    }
                }
            } else {
                throw new GuessException("Guess colors must be one of these: \"RED\", \"BLUE\", \"YELLOW\", \"ORANGE\", \"GREEN\", \"PURPLE\"");
            }
        }

        return new GuessResult(blackKeyPegs,whiteKeyPegs);
    }



    /**
     * Checks if a given color is a valid
     * @param color
     * @return True if the color is valid
     */
    private boolean isValidGuessColor(String color) {
        return Arrays.stream(colors).anyMatch(color::equals);
    }
}
