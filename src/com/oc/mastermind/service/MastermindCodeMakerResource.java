package com.oc.mastermind.service;


import com.oc.mastermind.core.GuessException;
import com.oc.mastermind.core.GuessResult;
import com.oc.mastermind.core.MastermindCodeMaker;
import com.oc.mastermind.core.SimpleMastermindCodeMaker;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;


@Path("/")
public class MastermindCodeMakerResource {

    private final MastermindCodeMaker mastermindCodeMaker = new SimpleMastermindCodeMaker();

    @GET
    @Path("/ping")
    public Response ping() {
        return Response.ok().entity("Service online").build();
    }

    @GET
    @Path("/new-game")
    @Produces("application/json")
    public Response createGame() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        String gameId = mastermindCodeMaker.createGame();
        jsonObject.put("gameid", gameId);

        return Response.status(200).entity(jsonObject.toString()).build();
    }

    @POST
    @Path("/guess")
    @Consumes("application/json")
    @Produces("application/json")
    public Response guess(final GuessRequestBean input) throws JSONException {

        JSONObject jsonObject = new JSONObject();
        try {
            if(input.gameid==null){
                return Response.status(400).entity("Invalid guess: gameid is null").build();
            }
            if(input.code==null){
                return Response.status(400).entity("Invalid code: code is null").build();
            }
            GuessResult result = mastermindCodeMaker.evaluateGuess(input.gameid,input.code);
            jsonObject.put("blacks", result.getBlackKeyPegs());
            jsonObject.put("whites", result.getWhiteKeyPegs());
            jsonObject.put("result", result.toString());

            return Response.status(200).entity(jsonObject.toString()).build();
        } catch (GuessException e) {
            e.printStackTrace();
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @GET
    @Path("/history/{gameid}")
    @Produces("application/json")
    public Response getHistoryLog(@PathParam("gameid") String gameId) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        try {
            JSONArray historyLogJSONArray = new JSONArray();
            mastermindCodeMaker.getHistoryLog(gameId).forEach(item -> historyLogJSONArray.put(item));
            jsonObject.put("log", historyLogJSONArray);
            jsonObject.put("log-str", mastermindCodeMaker.getHistoryLogString(gameId));
            return Response.status(200).entity(jsonObject.toString()).build();
        } catch (GuessException e) {
            e.printStackTrace();
            return Response.status(400).entity(e.getMessage()).build();
        }

    }

}
