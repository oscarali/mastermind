package com.oc.mastermind.service;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class GuessRequestBean {
    @XmlElement public String gameid;
    @XmlElement public String[] code;
}
